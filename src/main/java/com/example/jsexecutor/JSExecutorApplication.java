package com.example.jsexecutor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JSExecutorApplication {

	public static void main(String[] args) {
		SpringApplication.run(JSExecutorApplication.class, args);
	}

}
